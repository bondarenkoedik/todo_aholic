"use strict"

const express        = require("express"),
      mongoose       = require("mongoose"),
      morgan         = require("morgan"),
      bodyParser     = require("body-parser"),
      methodOverride = require("method-override");

const app = express();

module.exports = app;


// configuration

mongoose.connect("mongodb://localhost/todo_app");

app.use("/static", express.static(`${__dirname}/public`));
app.use(morgan("dev"));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(bodyParser.json({type: "application/vnd.api+json"}));
app.use(methodOverride("_method"));

app.set("view engine", "pug");


// model

const TodoSchema = new mongoose.Schema({
  text: String,
  done: { type: Boolean, default: false }
});

const Todo = mongoose.model("Todo", TodoSchema);


// routes

app.get("/", (req, res) => {
  res.render("index");
});

// TODO API

app.get("/api/todos", (req, res) => {
  
  Todo.find({}, (err, todos) => {
  
    if (err) {
      return res.send(err);
    }
    
    return res.json(todos);
  
  });

});


app.post("/api/todos", (req, res) => {
  
  Todo.create({

    text: req.body.text,
    done: false

  }, (err, obj) => {

    if (err)
      return res.send(err);

    Todo.find({}, (err, todos) => {
      
      if (err)
        return res.send(err);

      return res.json(todos);
    });

  });

});


app.delete("/api/todos/:todo_id", (req, res) => {
  
  Todo.findByIdAndRemove(req.params.todo_id, (err) => {

    if (err)
      return res.send(err);

    Todo.find({}, (err, todos) => {
      
      if (err)
        return res.send(err)

      return res.json(todos);
    
    });

  });

});


// listen
app.listen("3000", () => console.log("Server is running.."));