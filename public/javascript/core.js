(function() {

  "use strict"

  var todoApp = angular.module("todoApp", []);

  todoApp.controller("mainController", ["$scope", "$http", function($scope, $http) {
    
    $scope.task = "";

    // get all task
    $http.get("/api/todos")
      .success(function(data) {
        $scope.todos = data;
      })
      .error(function(data) {
        console.log("Error: " + data);
      });

    $scope.addTask = function() {
      if ($scope.task) {
        
        $http.post("/api/todos", {text: $scope.task})
          .success(function(data) {
            $scope.task = "";
            $scope.todos = data;
          })
          .error(function(data) {
            console.log("Error: " + data);
          });
      }
    };

    $scope.deleteTask = function(id) {
      
      $http.delete("/api/todos/"+id)
        .success(function(data) {
          $scope.todos = data;
        })
        .error(function(data) {
          console.log("Error: " + data);
        });
    
    }

  }]);
}());
